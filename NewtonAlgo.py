import pandas

from echelon3 import *
import SofaRuntime

##############################
# gradiant functions
##############################

def euclideanDistanceSquare(a,b):
    distance = [(a[i]-b[i])**2 for i in range(len(a))]
    return sum(distance)

def getError(distances):
    return sum(distances)/len(distances)

def partialDerivate(f,step,paramsFix,paramStudy,posReal,index,root):
    """gradient descent with one parameters (partoa) """
    # gradient descent with one parameters
    variables = paramsFix[0:index]+[paramStudy]+paramsFix[index:]
    posesSim = getPosSim(root,variables[0],variables[1],variables[2],variables[3])
    distances = [euclideanDistanceSquare(posesSim[i],posReal[i]) for i in range(len(posesSim))]
    fNext =  getError(distances)
    deriv = (fNext - f)/step
    return deriv

def newton(f,epsilon,maxIteration,params,steps ,posReal,root):
    bestParams = [params,error]
    for n in range(maxIteration):
       
        if abs(f)<epsilon:
            print('Found solution after',n,'iterations.')
            return params
        
        derivs = []
        for i in range(len(params)):
            deriv = partialDerivate(f = f,step = steps[i],paramsFix= params[0:i]+params[i+1:],paramStudy=params[i]+steps[i],posReal= posReal,index = i, root = root)     
            derivs.append(deriv)
            if deriv ==0:
                print('Zero derivative. No solution found.')
                return None  

        for i in range(1):
            steps[i] = f/derivs[i]
            params[i] -=  min(1e8,steps[i])

        for i in range(1,2):
            steps[i] = f/derivs[i]
            params[i] -=  min(1e6,steps[i])

        for i in range(2,4):
            steps[i] = f/derivs[i]
            params[i] -=  min(1e-3,steps[i])

        posesSim = getPosSim(root,params[0],params[1],params[2],params[3])
        distances = [euclideanDistanceSquare(posesSim[i],posReal[i]) for i in range(len(posesSim))]
        error =  getError(distances)

        if error < bestParams[1]:
            bestParams = [params,error]

        f = error
        print('Step :',n,", error = ",error," mm²" )
        print(params,'\n')

    print('Exceeded maximum iterations. No good solution found.')
    return bestParams

##############################
# Sofa functions
##############################

def getPosSim(root,youngModulusRibs,youngModulusTube,volumMassRibs,volumMassTube):

    # Reset simulation
    Sofa.Simulation.reset(root)
    root.Echelon.SimuController.endSim = False
    root.Echelon.SimuController.effectorPosition = []

    # change parameters
    root.Echelon.framesNode.topoRibs.BeamInterpolation.defaultYoungModulus = youngModulusRibs
    root.Echelon.framesNode.topoRibs.AdaptiveBeamForceFieldAndMass.massDensity = volumMassRibs
    root.Echelon.framesNode.topoTube.tubeInterpolation.defaultYoungModulus = youngModulusTube
    root.Echelon.framesNode.topoTube.AdaptiveBeamForceFieldAndMass.massDensity = volumMassTube

    # run the simulation until you have all positions
    while root.Echelon.SimuController.endSim != True: 
        Sofa.Simulation.animate(root, root.dt.value)
        Sofa.Simulation.updateVisual(root)

    return root.Echelon.SimuController.effectorPosition

##############################
# get real datas from csv file
##############################

def getRealDatas(file):
    datas = pandas.read_csv(file)
    posReal = [[datas["x(mm)"][i],datas["y(mm)"][i],datas["z(mm)"][i]] for i in range(datas.shape[0])]
    forces = [datas["force(g)"][i] for i in range(datas.shape[0])]
    return posReal,forces

##############################
# Optimization functions
##############################

def main():

    # forces to studies
    posReal,forces = getRealDatas('EffectorPose.csv')

    # init parameters

    maxIteration = 10

    youngModulusRibs = 3.5e9
    youngModulusTube = 1e7
    volumMassRibs = 0.001
    volumMassTube = 0.0005

    stepYoungRibs = 1e8
    stepYoungTube = 1e6
    stepVolumRibs = 0.001
    stepVolumTube = 0.0001


    params = [youngModulusRibs,youngModulusTube,volumMassRibs,volumMassTube]
    steps = [stepYoungRibs,stepYoungTube,stepVolumRibs,stepVolumTube]

    errorMin = 100

    # create Scene only once
    # add Path to plugins
    SofaRuntime.PluginRepository.addFirstPath('/home/alessandrini/Repos/Code/sofa/build/external_directories')

    # Make sure to load all SOFA libraries
    SofaRuntime.importPlugin("SofaBaseMechanics")
    SofaRuntime.importPlugin("SofaOpenglVisual")

    #Create the root node 
    root = Sofa.Core.Node("root")
    
    # Call the below 'createScene' function to create the scene graph
    createScene(root,forces,params[0],params[1],params[2],params[3])
    Sofa.Simulation.init(root)

    # get first error
    posesSim = getPosSim(root,params[0],params[1],params[2],params[3])

    distances = [euclideanDistanceSquare(posesSim[i],posReal[i]) for i in range(len(posesSim))]
    error =  getError(distances)

    bestParams  = newton(f = error,
                         epsilon = errorMin,
                         maxIteration= maxIteration,
                         params = params,
                         steps = steps, 
                         posReal = posReal,
                         root = root)

    print(bestParams)


main()
