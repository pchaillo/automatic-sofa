import pandas
from sklearn.exceptions import PositiveSpectrumWarning

from echelon3 import *
import SofaRuntime

##############################
# gradiant functions
##############################

def euclideanDistanceSquare(a,b):
    distance = [(a[i]-b[i])**2 for i in range(len(a))]
    return sum(distance)

def getError(distances):
    return sum(distances)/len(distances)

def gradientOneVariable(f,step,paramsFix,paramStudy,posReal,index,root):
    """gradient descent with one parameters (partoa) """
    # gradient descent with one parameters
    variables = paramsFix[0:index]+[paramStudy]+paramsFix[index:]
    posesSim = getPosSim(root,variables[0],variables[1],variables[2],variables[3])
    distances = [euclideanDistanceSquare(posesSim[i],posReal[i]) for i in range(len(posesSim))]
    fNext =  getError(distances)
    deriv = (fNext-f)/step
    return deriv

def gradientDescent(error,errorMin,params, steps,alphas,posReal,root):
    step = 0
    
    while error > errorMin :
        step = step + 1
        for i in range(len(params)):
            deriv = gradientOneVariable(f = error,step = steps[i],paramsFix= params[0:i]+params[i+1:],paramStudy=params[i]+steps[i],posReal= posReal,index = i, root = root)

            steps[i] = deriv*alphas[i]
        for i in range(len(params)):
            print(params[i],steps[i])
            params[i] -=  steps[i]

        posesSim = getPosSim(root,params[0],params[1],params[2],params[3])
        distances = [euclideanDistanceSquare(posesSim[i],posReal[i]) for i in range(len(posesSim))]
        error =  getError(distances)

        print('Step : ',step,", error = ",error," mm²" )
        print(params,'\n')
    return params, error

##############################
# Sofa functions
##############################

def getPosSim(root,youngModulusRibs,youngModulusTube,volumMassRibs,volumMassTube):

    # Reset simulation
    Sofa.Simulation.reset(root)
    root.Echelon.SimuController.endSim = False
    root.Echelon.framesNode.constraintPoints.Section1Cable1.value.value = [0]
    root.Echelon.SimuController.EndEffectorPositions = []
    root.Echelon.SimuController.n = 0

    # change parameters
    root.Echelon.framesNode.topoRibs.BeamInterpolation.defaultYoungModulus = youngModulusRibs
    root.Echelon.framesNode.topoRibs.AdaptiveBeamForceFieldAndMass.massDensity = volumMassRibs
    root.Echelon.framesNode.topoTube.tubeInterpolation.defaultYoungModulus = youngModulusTube
    root.Echelon.framesNode.topoTube.AdaptiveBeamForceFieldAndMass.massDensity = volumMassTube

    # run the simulation until you have all positions
    while root.Echelon.SimuController.endSim != True: 
        Sofa.Simulation.animate(root, root.dt.value)
        Sofa.Simulation.updateVisual(root)

    return root.Echelon.SimuController.EndEffectorPositions

##############################
# get real datas from csv file
##############################

def getRealDatas(file):
    datas = pandas.read_csv(file)
    posReal = [[datas["x(mm)"][i],datas["y(mm)"][i],datas["z(mm)"][i]] for i in range(datas.shape[0])]
    forces = [datas["force(g)"][i] for i in range(datas.shape[0])]
    return posReal,forces

##############################
# Optimization functions
##############################

def main():

    # forces to studies
    posReal,forces = getRealDatas('EffectorPose.csv')

    # init parameters

    youngModulusRibs = 1e8
    youngModulusTube = 1e7
    volumMassRibs = 1e-3
    volumMassTube = 5e-4

    stepYoungRibs = 1e7
    stepYoungTube = 1e6
    stepVolumRibs = 1e-4
    stepVolumTube = 1e-5

    alphaYoungRibs = 1e11
    alphaYoungTube = 1e8
    alphaVolumRibs = 1e-12
    alphaVolumTube = 1e-13

    params = [youngModulusRibs,youngModulusTube,volumMassRibs,volumMassTube]
    steps = [stepYoungRibs,stepYoungTube,stepVolumRibs,stepVolumTube]
    alphas = [alphaYoungRibs,alphaYoungTube,alphaVolumRibs,alphaVolumTube]

    errorMin = 100

    # create Scene only once
    # add Path to plugins
    SofaRuntime.PluginRepository.addFirstPath('/home/alessandrini/Repos/Code/sofa/build/external_directories')

    # Make sure to load all SOFA libraries
    SofaRuntime.importPlugin("SofaBaseMechanics")
    SofaRuntime.importPlugin("SofaOpenglVisual")

    #Create the root node 
    root = Sofa.Core.Node("root")
    
    # Call the below 'createScene' function to create the scene graph
    createScene(root,forces,params[0],params[1],params[2],params[3])
    Sofa.Simulation.init(root)

    # get first error
    posesSim = getPosSim(root,params[0],params[1],params[2],params[3])

    distances = [euclideanDistanceSquare(posesSim[i],posReal[i]) for i in range(len(posesSim))]
    error =  getError(distances)

    params, error  = gradientDescent(error,
                                    errorMin = errorMin,
                                    params = params,
                                    steps = steps, 
                                    alphas = alphas,
                                    posReal = posReal,
                                    root = root)

    # print(params, error)


main()
