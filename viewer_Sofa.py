#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 21 09:51:24 2021

@author: tanguy
"""

import pygame

import numpy as np
import math

import OpenGL.GL as GL
import OpenGL.GLU as GLU

import Sofa
import Sofa.SofaGL

import vtk


display_size = (1200, 900)



# Init window for display
def init_display(node, display_size=display_size):
    """ Init pygame window for in real time Sofa simulation display from python script
    
    Inputs:
    ---------
        display_size: tuple of int
            Size of the window to display scene
        node: Sofa node 
            Root node form Sofa simulation
            
    Outputs:
    --------
       None """
    
    pygame.display.init()
    pygame.display.set_mode(display_size, pygame.DOUBLEBUF | pygame.OPENGL)

    GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)
    GL.glEnable(GL.GL_LIGHTING)
    GL.glEnable(GL.GL_DEPTH_TEST)
    Sofa.SofaGL.glewInit()
    Sofa.Simulation.initVisual(node)
    Sofa.Simulation.initTextures(node)
    GL.glMatrixMode(GL.GL_PROJECTION)
    GL.glLoadIdentity()
    GLU.gluPerspective(45, (display_size[0] / display_size[1]), 0.1, 500.0)
    GL.glMatrixMode(GL.GL_MODELVIEW)
    GL.glLoadIdentity()


# Renderer
def simple_render(node, display_size=display_size):
    """ Get the Open GL context to render an image of the current state
    
    Inputs:
    ---------
        display_size: tuple of int
            Size of the window to display scene
        node: Sofa node 
            Root node form Sofa simulation
            
    Outputs:
    --------
       None """

    # print('stupid_test') # la fonction est bien executée
    
    GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)
    GL.glEnable(GL.GL_LIGHTING)
    GL.glEnable(GL.GL_DEPTH_TEST)
    GL.glMatrixMode(GL.GL_PROJECTION)
    GL.glLoadIdentity()
    GLU.gluPerspective(45, (display_size[0] / display_size[1]), 0.1, 500.0)
    GL.glMatrixMode(GL.GL_MODELVIEW)
    GL.glLoadIdentity()
    cameraMVM = node.camera.getOpenGLModelViewMatrix()
    GL.glMultMatrixd(cameraMVM)
    Sofa.SofaGL.draw(node)
    pygame.display.flip()
    

def bounding_box(path_vtk):
    """ Compute the bounding box of a mesh
    
    Inputs:
    ---------
        path_vtk: string
            Path to vtk file of mesh
    
    Outputs:
    --------
       bboxs: list of numpy arrays 
           Coordinates of two extremum points from bounding box """
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(path_vtk)
    reader.ReadAllVectorsOn()
    reader.ReadAllScalarsOn()
    reader.Update()
    mesh = reader.GetOutput()

    # Matrix of point coordinates
    coordinates = np.array(mesh.GetPoints().GetData())

    min_x, min_y, min_z = np.min(coordinates, axis=0)
    max_x, max_y, max_z = np.max(coordinates, axis=0)
    
    return([np.array([min_x, min_y, min_z]), np.array([max_x, max_y, max_z])])
    

def stop_render():
    pygame.display.quit()
    

def camera_position(bbox, fov, view_axis=None):
    """ Compute the camera position to visualize a given scene
    
    Inputs:
    ---------
        bbox: list of numpy arrays
            Coordinates of two extremum of the boudning box
        fov = float
            Field of view in degrees
        view_axis = string in{x, -x, y, -y, z, -z}
            Axis to set camera, computed automatically if no axis is given
    
    Outputs:
    --------
       deformation_gradients: list of numpy arrays 
       Deformation gradients ordered by face index """
    
    def euclidean_distance(p1, p2):
        """ Comptue the euclidean distace between two points """
        return np.sqrt(np.dot(p2-p1,p2-p1))
    
    center = bbox[0] + ((bbox[1] - bbox[0]) / 2 )
    
    # Compute corners of bounding box 
    corner_bl_z1 = np.array([bbox[0][0], bbox[0][1], bbox[0][2]])
    corner_tl_z1 = np.array([bbox[0][0], bbox[1][1], bbox[0][2]])
    corner_br_z1 = np.array([bbox[1][0], bbox[0][1], bbox[0][2]])
    corner_tr_z1 = np.array([bbox[1][0], bbox[1][1], bbox[0][2]])
    corner_bl_z2 = np.array([bbox[0][0], bbox[0][1], bbox[1][2]])
    corner_tl_z2 = np.array([bbox[0][0], bbox[1][1], bbox[1][2]])
    corner_br_z2 = np.array([bbox[1][0], bbox[0][1], bbox[1][2]])
    corner_tr_z2 = np.array([bbox[1][0], bbox[1][1], bbox[1][2]])
    corners = [corner_bl_z1, corner_tl_z1, corner_br_z1, corner_tr_z1, corner_bl_z2, corner_tl_z2, corner_br_z2, corner_tr_z2]
    
    # Compute area of faces
    area_face_x = euclidean_distance(corner_bl_z1, corner_bl_z2) * euclidean_distance(corner_bl_z1, corner_tl_z1) 
    area_face_y = euclidean_distance(corner_bl_z1, corner_bl_z2) * euclidean_distance(corner_bl_z1, corner_br_z1)
    area_face_z = euclidean_distance(corner_bl_z1, corner_tl_z1) * euclidean_distance(corner_bl_z1, corner_br_z1) 
    area_faces = [area_face_x, area_face_y, area_face_z]
    
    # Positioning of camera
    value = 1
    if view_axis == "x":
        view_axis_id = 0
    elif view_axis == "-x":
        view_axis_id = 0
        value = -1
    elif view_axis == "y":
        view_axis_id = 1
    elif view_axis == "-y":
        view_axis_id = 1
        value = -1
    elif view_axis == "z":
        view_axis_id = 2
    elif view_axis == "-z":
        view_axis_id = 2
        value = -1
    else:
        # Positioning of camera at perpendicular to center of largest bbox face
        view_axis_id = area_faces.index(max(area_faces)) #0=>x, 1=>y, 2=>z  
        
    axis_camera = [0,0,0]
    axis_camera[view_axis_id] = value
    axis_camera = np.array(axis_camera)
            
    # Find radius of bouding sphere
    bsphere_r = 0
    for corner in corners:
        dist_to_center = euclidean_distance(corner, center)
        if dist_to_center > bsphere_r:
            bsphere_r = dist_to_center
    
    # Camera distance to fit boundign sphere knowing fov
    fov = math.radians(fov) 
    cam_dist_to_centre = (bsphere_r) / math.tan(fov / 2)
    
    # Final positioning of camera
    cam_position = center - (axis_camera * cam_dist_to_centre)
    
    return(cam_position, center, cam_dist_to_centre)
    
