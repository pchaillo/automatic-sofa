
#print("simple_test")

# Lancer Sofa 3 fois avec une scène vide ?

import Sofa
import time
import SimuVal as Sim
import viewer_Sofa as viewer
import numpy as np
from scipy.spatial import distance

from ucl_collaboration import *
from ucl_collaboration.stiff_module import *

# from Scene_exemple import *

### PARAM7TRES ####

# nb_scene = 3

###################

def scene_autosofa(param_i):
    step = 0
    iter_sim = 0
    ecart_tab = []
    for i in range(Sim.nb_scene):
        print("\n Scene SOFA  n :  " + str(i))
                
        # SOFA simulation
        root = Sofa.Core.Node("root") # Generate the root node 
        # MyScene(root,step) # for Scene_exemple 
        MyScene(root,out_flag = 1,step =step,YM_soft_part = param_i ,coef_poi = 0.3870) # Create the scene graph
        Sofa.Simulation.init(root) # Initialization of the scene graph
        viewer.init_display(root)

        # print(dir(root))
        
        # Don't do anything until end of simulation
        while root.simu_controller.FinSimu != True: #SImuDOne is a variable udpated with the controller when the simulation is finished
             Sofa.Simulation.animate(root, root.dt.value)
             Sofa.Simulation.updateVisual(root)
             viewer.simple_render(root)
             time.sleep(root.dt.value)
             iter_sim += 1  

             # print('~~~~~~~~~~~~' + str(root.simu_pos_compar.ecart)) 
             ecart_tab.append(root.simu_pos_compar.ecart)
                
        # Reset simulation
        Sofa.Simulation.reset(root)
        step += 1  
        # print('Tableau de tous les écart enregistrés : ' + str(ecart_tab))
    return ecart_tab

def parametre_test(param_init,param_new,distance_new,distance_initale):
    if distance_new < distance_initale :
        delta = param_new - param_init
        # flag = 1
    else :
        # flag = -1
        delta = (param_init-param_new)*0.8 # 2 ?
        
    return delta


def reccur_parametre(param_i,pos_i,pos_d, delta_i,erreur,nb_iter,nb_iter_max):
    nb_iter += 1

    param_new = param_i + delta_i
    ecart_tab = scene_autosofa(param_new)
    ecart_mean = np.mean(ecart_tab)
    # ecart_median = np.median(ecart_tab)
    # print('Ecart moyen : ' +str(ecart_mean))
    # print('Ecart median : ' +str(ecart_median))
    pos_new = ecart_mean
    distance_initale = distance.euclidean(pos_i, pos_d)
    distance_new = distance.euclidean(pos_new, pos_d)
    # if distance_new < dist_best :
    #     dist_best = distance_new

    print("====================================================")
    print("====================================================")
    print("==== NOUVEAU  param : "+ str(param_new) + "=====")
    print("distance_new = " + str(distance_new))
    print("itération n° : " + str(nb_iter))
    print("====================================================")
    print("====================================================")
    # print("delta for stop :" + str(delta_i))
    if distance_new < erreur or nb_iter >= nb_iter_max or abs(delta_i) < 0.001 :
        return param_new
    elif distance_new > 20 :
        param_new = -100
        return param_new
    else :
        delta = parametre_test(param_i,param_new,distance_new,distance_initale)
        print("param = " + str(param_new))
        print("delta = " + str(delta))
        param_fin = reccur_parametre(param_new,pos_new,pos_d,delta,erreur,nb_iter,nb_iter_max)
        return param_fin


# global step 
# global iter_sim

param_i = 60
ecart_tab = scene_autosofa(param_i)
pos_i = np.mean(ecart_tab)
pos_d = 0
delta_i = 10
erreur = 10
# dist_best = pos_i
nb_iter = 0
nb_iter_max = 5
parametre_estime = reccur_parametre(param_i,pos_i,pos_d, delta_i,erreur,nb_iter,nb_iter_max)#,dist_best)
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("~~~ Nouveau paramètre : " + str(parametre_estime)+ "~~~")
print("~~~ Erreur acceptée : " + str(erreur)+ "~~~")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

if parametre_estime == -100 :
    print("Attention, arrété à cause des divergences")








