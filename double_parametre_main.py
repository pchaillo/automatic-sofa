# double dichotomie en bascule qui alterne entre les deux paramètres : pas opti du tout, converge mal mais fonctionne

#print("simple_test")

# Lancer Sofa 3 fois avec une scène vide ?

import Sofa
import time
import SimuVal as Sim
import viewer_Sofa as viewer
import numpy as np
from scipy.spatial import distance
from os import getcwd, chdir, mkdir
from datetime import datetime
import time

from ucl_collaboration import *
from ucl_collaboration.stiff_module import *

# from Scene_exemple import *

### PARAM7TRES ####

# nb_scene = 3

###################

def scene_autosofa(param_a,param_b,param_flag):
    step = 0
    iter_sim = 0
    ecart_tab = []

    if param_flag == 0:
        param_YM = param_a 
        param_CP = param_b
    else :
        param_YM = param_b 
        param_CP = param_a

    for i in range(Sim.nb_scene):
        print("\n Scene SOFA  n :  " + str(i))
                
        # SOFA simulation
        root = Sofa.Core.Node("root") # Generate the root node 
        # MyScene(root,step) # for Scene_exemple 
        MyScene(root,out_flag = 1,step =step,YM_soft_part = param_YM,coef_poi = param_CP) # Create the scene graph
        Sofa.Simulation.init(root) # Initialization of the scene graph
        viewer.init_display(root)

        # print(dir(root))
        
        # Don't do anything until end of simulation
        while root.simu_controller.FinSimu != True: #SImuDOne is a variable udpated with the controller when the simulation is finished
             Sofa.Simulation.animate(root, root.dt.value)
             Sofa.Simulation.updateVisual(root)
             viewer.simple_render(root)
             time.sleep(root.dt.value)
             iter_sim += 1  

             # print('~~~~~~~~~~~~' + str(root.simu_pos_compar.ecart)) 
             ecart_tab.append(root.simu_pos_compar.ecart)
                
        # Reset simulation
        Sofa.Simulation.reset(root)
        step += 1  
        # print('Tableau de tous les écart enregistrés : ' + str(ecart_tab))
    return ecart_tab

def parametre_test(param_init,param_new,distance_new,distance_initale):
    # fonction qui calcule le nouveau delta (écart entre deux paramètres) en fct de l'erreur realtive à ces paramètres
    print("distance_new ::" + str(distance_new))
    print("distance_init ::" + str(distance_initale))

    if distance_new < distance_initale :
        delta = param_new - param_init
        # flag = 1
    else :
        # flag = -1
        delta = (param_init-param_new)*0.6 # 2 ?
        
    return delta


def erreur_recup(ecart_tab): # détermine l'erreur que je vais prendre en compte par la suite, dans une fonction à part pour être sûr que ce soit la même méthode utilisée partout
    # pos_new = np.mean(ecart_tab)
    # pos_new = np.median(ecart_tab)
    pos_new = max(ecart_tab)

    return pos_new


def reccur_parametre(param_i_aa,param_i_bb,pos_i,pos_d, delta_i_aa,delta_i_bb,erreur,nb_iter,nb_iter_max,param_flag):
    # fonction réccursive, qui se mlance en boucle jusqu'à que l'erreur soit plus petite que le seuil choisi ou que le max d'itération soit atteint
    nb_iter += 1

    if param_flag == 0 : # pour choisr lequel des deux paramètres je vais varier
        param_i = param_i_aa
        delta_i = delta_i_aa
        param_b = param_i_bb
    else :
        param_i = param_i_bb
        delta_i = delta_i_bb
        param_b = param_i_aa

    param_new = param_i + delta_i
    ecart_tab = scene_autosofa(param_new,param_b,param_flag) # lancer une simu et renvoie le tableau des écarts entre les positions désirées et les positions atteintes
    pos_new = erreur_recup(ecart_tab)
    distance_initale = distance.euclidean(pos_i, pos_d)
    distance_new = distance.euclidean(pos_new, pos_d)
    # if distance_new < dist_best :
    #     dist_best = distance_new

    print("====================================================")
    print("====================================================")
    print("==== NOUVEAU  param : "+ str(param_new) + "=====")
    print("distance_new = " + str(distance_new))
    print("itération n° : " + str(nb_iter))
    print("====================================================")
    print("====================================================")
    if distance_new < erreur or nb_iter >= nb_iter_max or abs(delta_i) < 0.001: # si delta est trop petit, je considère que le système a convergé vers sa solution optimale, meme si l'erreur n'a pas été atteinte
        return [param_new,distance_new]
    elif distance_new > 60 :
        param_new = -100
        return [param_new,distance_new]
    else :
        delta = parametre_test(param_i,param_new,distance_new,distance_initale)
        print("param = " + str(param_new))
        print("delta = " + str(delta))
        if param_flag == 0 : 
            param_i_aa = param_new
            delta_i_aa = delta
            # param_i_bb = param_b
        else :
            param_i_bb = param_new
            delta_i_bb = delta
            # param_i_aa = param_b
        [param_fin ,distance_fin] = reccur_parametre(param_i_aa,param_i_bb,pos_new,pos_d, delta_i_aa,delta_i_bb,erreur,nb_iter,nb_iter_max,param_flag)
        return [param_fin, distance_fin]

# la classe n'est pas une mauvaise idée en soit, mais rajoute un argument à des fonctions avec déjà pas mal d'arguments, je pense qu'ici ce n'est pas la bonne solution
class best_param_recorder():
    def __init__(self):
        self.path = getcwd()
        d_et_h = str(datetime.now())
        nf = self.path+'/record/best_param_record_' + d_et_h[0:19] + '.txt'
        self.fichier_txt = open(nf,'x')

    def record_best(self,best_param_a,best_erreur_a,best_param_b,best_erreur_b,i_boucle):
        self.fichier_txt.write('\n ----------- Boucle numéro : ' + str(i_boucle) + ' ---------')
        self.fichier_txt.write('\n Nouveau paramètre a : ' + str(best_param_a))
        self.fichier_txt.write('\n Erreur acceptée a : ' + str(best_erreur_a))
        self.fichier_txt.write('\n Nouveau paramètre b : ' + str(best_param_b))
        self.fichier_txt.write('\n Erreur acceptée b : ' + str(best_erreur_b))
        self.fichier_txt.write('\n -----------  ---------')


# def record_init():
#         path = getcwd()
#         d_et_h = str(datetime.now())
#         nf = path+'/record/best_param_record_' + d_et_h[0:19] + '.csv'
#         self.fichier_csv = open(nf,'x')

# def record_best(best_param_a,best_erreur_a,best_param_b,best_erreur_b)):


def best_param_printer(best_param_a,best_erreur_a,best_param_b,best_erreur_b):
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("~~~ Nouveau paramètre a : " + str(best_param_a)+ "~~~")
    print("~~~ Erreur acceptée a : " + str(best_erreur_a)+ "~~~")
    print("~~~ Nouveau paramètre b : " + str(best_param_b)+ "~~~")
    print("~~~ Erreur acceptée b : " + str(best_erreur_b)+ "~~~")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")


def double_parametre(param_i_aa,param_i_bb,pos_i,pos_d, delta_i_aa,delta_i_bb,erreur,nb_iter,nb_iter_max,param_flag,nb_boucle,record):#,param_flag):r
# oscille entre deux paramètres à faire varier, en gardant la meilleure v(celui ou l'erreur est moindre)
    best_erreur_a = pos_i
    best_erreur_b = pos_i
    best_param_a = param_i_aa
    best_param_b = param_i_bb
    for i in range(nb_boucle):
        print("-------------------------------------------")
        print("-------------------------------------------")
        print("-------------------------------------------")
        print("-------------------------------------------")
        print("Boucle numéro : " + str(i+1))
        print("-------------------------------------------")
        print("-------------------------------------------")
        print("-------------------------------------------")
        print("-------------------------------------------")

        if param_flag == 0 :
            [param_i_aa, erreur_aa]  = reccur_parametre(param_i_aa,param_i_bb,pos_i,pos_d, delta_i_aa,delta_i_bb,erreur,nb_iter,nb_iter_max,param_flag)
            if param_i_aa == -100 : # si la simu est instable, je diminue le pas pour que les nouveaux paramètres n'atteignent pas cette instabilité
                param_i_aa = best_param_a
                delta_i_aa = delta_i_aa*0.6
            elif erreur_aa < best_erreur_a : # si une valeur trouvée diminue l'erreur, je l'enregistre en tant que nouvelle valeur
                best_param_a = param_i_aa
                best_erreur_a = erreur_aa
                best_param_printer(best_param_a,best_erreur_a,best_param_b,best_erreur_b)
                record.record_best(best_param_a,best_erreur_a,best_param_b,best_erreur_b,i)
            else : 
                param_i_aa = best_param_a
                delta_i_aa = delta_i_aa*0.6

            param_flag = 1
        else :
            # print("&&&&&&&&&&&&&&&& - test - &&&&&&&&&&&&&&")
            [param_i_bb, erreur_bb] = reccur_parametre(param_i_aa,param_i_bb,pos_i,pos_d, delta_i_aa,delta_i_bb,erreur,nb_iter,nb_iter_max,param_flag)
            if param_i_bb == -100 :
                param_i_bb = best_param_b
                delta_i_bb = delta_i_bb*0.6
            elif erreur_bb < best_erreur_b :
                best_param_b = param_i_bb
                best_erreur_b = erreur_bb
                best_param_printer(best_param_a,best_erreur_a,best_param_b,best_erreur_b)
                record.record_best(best_param_a,best_erreur_a,best_param_b,best_erreur_b,i)
            else : # tentative pour améliorer la convergence
                param_i_bb = best_param_b
                delta_i_bb = delta_i_bb*0.6
            param_flag = 0
    return [best_param_a,best_param_b,best_erreur_a,best_erreur_b]

# global step 
# global iter_sim

# start_time = time.time()

param_flag = 0 # 0 => YM // 1 => coef_poi

param_i_aa = 100
param_i_bb = 0.1
ecart_tab = scene_autosofa(param_i_aa,param_i_bb,param_flag)
pos_i = erreur_recup(ecart_tab)
pos_d = 0
delta_i_aa = 50
delta_i_bb = 0.2
erreur = 5
# dist_best = pos_i
nb_iter = 0
nb_iter_max = 10
nb_boucle = 2 # nb d boucle Ym puis Cp à effectuer (2=1ym+1cp)

record = best_param_recorder()

print("Ecart initial : " + str(pos_i))
# parametre_estime = reccur_parametre(param_i,pos_i,pos_d, delta_i,erreur,nb_iter,nb_iter_max)#,dist_best)
[best_param_a,best_param_b,best_erreur_a,best_erreur_b] = double_parametre(param_i_aa,param_i_bb,pos_i,pos_d, delta_i_aa,delta_i_bb,erreur,nb_iter,nb_iter_max,param_flag,nb_boucle,record)
# print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
# print("~~~ Nouveau paramètre : " + str(parametre_estime)+ "~~~")
# print("~~~ Erreur acceptée : " + str(erreur)+ "~~~")
# print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

best_param_printer(best_param_a,best_erreur_a,best_param_b,best_erreur_b)

# interval = time.time() - start_time
# print('Total time in seconds:'+ str(interval) )