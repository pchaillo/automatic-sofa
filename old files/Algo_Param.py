#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: pchaillo
"""

from scipy.spatial import distance

# # exemple de calcul de sitance euclidienne
# a = (1, 1, 1)
# b = (1, 1, 2)
# print(distance.euclidean(a, b))

def parametre_test(param_init,param_new,distance_new,distance_initale):
    if distance_new < distance_initale :
        delta = param_new - param_init
        # flag = 1
    else :
        # flag = -1
        delta = (param_init-param_new)/2
        
    return delta

def simulation(param_new):
    val = param_new*param_new
    return val


def reccur_parametre(param_i,pos_i,pos_d, delta_i,erreur):
    param_new = param_i + delta_i
    pos_new = simulation(param_new)
    distance_initale = distance.euclidean(pos_i, pos_d)
    distance_new = distance.euclidean(pos_new, pos_d)
    print("distance_new = " + str(distance_new))
    if distance_new < erreur :
        return param_new
    else :
        delta = parametre_test(param_i,param_new,distance_new,distance_initale)
        print("param = " + str(param_new))
        print("delta = " + str(delta))
        param_fin = reccur_parametre(param_new,pos_new,pos_d,delta,erreur)
        return param_fin
    

    
param_i = 1
pos_i = simulation(param_i)
erreur = 0.00000000000000000000001
delta_i = 3
pos_d = 4
valeur_estimee = reccur_parametre(param_i,pos_i,pos_d, delta_i,erreur)
print("Nouveau paramètre : ")
print(str(valeur_estimee))

    
    


    
    
    
    
    
    



