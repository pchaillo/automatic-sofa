
#print("simple_test")

# Lancer Sofa 3 fois avec une scène vide ?

import Sofa
import time
import SimuVal as Sim
import viewer_Sofa as viewer

### PARAM7TRES ####

# nb_scene = 3

###################


def createScene2(rootNode):

    # required plugins:
    rootNode.addObject('AddPluginRepository', path = '/home/pchaillo/Documents/10-SOFA/sofa/build/master/external_directories/plugins/SoftRobots/lib/') #libSoftRobots.so 1.0
    rootNode.addObject('AddPluginRepository', path = '/home/pchaillo/Documents/10-SOFA/sofa/build/master/external_directories/plugins/ModelOrderReduction/lib/') #libSoftRobots.so 1.0
    rootNode.addObject('AddPluginRepository', path = '/home/pchaillo/Documents/10-SOFA/sofa/build/master/external_directories/plugins/BeamAdapter/lib')#/libBeamAdapter.so 1.0

    rootNode.addObject('RequiredPlugin', name='SoftRobots')
    rootNode.addObject('RequiredPlugin', name='ModelOrderReduction')
    rootNode.addObject('RequiredPlugin', name='BeamAdapter')
    rootNode.addObject('RequiredPlugin', name='SofaConstraint')
    rootNode.addObject('RequiredPlugin', name='SofaDeformable')
    rootNode.addObject('RequiredPlugin', name='SofaGeneralAnimationLoop')
    rootNode.addObject('RequiredPlugin', name='SofaImplicitOdeSolver')
    rootNode.addObject('RequiredPlugin', name='SofaLoader')
    rootNode.addObject('RequiredPlugin', name='SofaMeshCollision')
    rootNode.addObject('RequiredPlugin', name='SofaSimpleFem')
    rootNode.addObject('RequiredPlugin', name='SofaSparseSolver')
    rootNode.addObject('RequiredPlugin', name='SofaEngine')


    #rootNode.addObject('RequiredPlugin', name='SofaPython')
    # rootNode.findData('gravity').value=[0, 0, 9810];
    rootNode.findData('gravity').value=[0, 0, 0];

        #visual dispaly
    rootNode.addObject('VisualStyle', displayFlags='showVisualModels showBehaviorModels showCollisionModels hideBoundingCollisionModels showForceFields showInteractionForceFields hideWireframe')
    rootNode.addObject('BackgroundSetting', color='0 0.168627 0.211765')
    #rootNode.createObject('OglSceneFrame', style="Arrows", alignment="TopRight")
    
    rootNode.findData('dt').value= 0.01;
    
    rootNode.addObject('FreeMotionAnimationLoop')
    rootNode.addObject('DefaultVisualManagerLoop')   
    # rootNode.addObject('QPInverseProblemSolver', name="QP", printLog='0', saveMatrices = True )
    rootNode.addObject('GenericConstraintSolver', maxIterations='100', tolerance = '0.0000001')

    #goal
    goal = rootNode.addChild('goal')
    goal.addObject('EulerImplicitSolver', firstOrder=True)
    goal.addObject('CGLinearSolver', iterations='1000',threshold="1e-5", tolerance="1e-5")
    # cible = goal.addObject('MechanicalObject', name='goalMO', position=[0, 0, h_effector])
    goal.addObject('MechanicalObject', name='goalMO', position=[iter_sim/100, 0,0])
    goal.addObject('SphereCollisionModel', radius='0.5')#, group='1')
    goal.addObject('UncoupledConstraintCorrection')

    rootNode.addObject("InteractiveCamera", name="camera", position=Sim.pos_tab[step])#, zNear=0.1, zFar=500, computeZClip = False,  projectionType=0)
    rootNode.addObject(simu_controller(name="simu_controller", RootNode=rootNode))#, Step=step))


    return rootNode


""" Controller for simulation """
class simu_controller(Sofa.Core.Controller):

    def __init__(self, *args, **kwargs):
        Sofa.Core.Controller.__init__(self, *args, **kwargs)
        
        self.RootNode = kwargs["RootNode"]
        
        self.IterSimu = 0 # Counter for dt steps before stopping simulation
        self.MaxIter = Sim.nb_iter# Number of dt steps before stopping simulation
        self.FinSimu = False # Variable to store if Simulation is done
        
        
    def onAnimateBeginEvent(self, dt): 

        # Compute error when simu is done            
        if (self.IterSimu >= self.MaxIter) :
            # self.compute_error()
            # with open(constants.OptimPath + "logs.txt","a") as logs:
            #     logs.write("\n")
            self.FinSimu = True 
        self.IterSimu += 1    

global step 
global iter_sim

step = 0
iter_sim = 0

for i in range(Sim.nb_scene):
    print("\n Scene SOFA  n :  " + str(i))
            
    # SOFA simulation
    root = Sofa.Core.Node("root") # Generate the root node     
    createScene2(root) # Create the scene graph
    Sofa.Simulation.init(root) # Initialization of the scene graph
    viewer.init_display(root)

    # print(dir(root))
    
    # Don't do anything until end of simulation
    while root.simu_controller.FinSimu != True: #SImuDOne is a variable udpated with the controller when the simulation is finished
         Sofa.Simulation.animate(root, root.dt.value)
         Sofa.Simulation.updateVisual(root)
         viewer.simple_render(root)
         time.sleep(root.dt.value)
         iter_sim += 1   
    
    # Reset simulation
    Sofa.Simulation.reset(root)
    
    # Load next PCL
    step += 1  