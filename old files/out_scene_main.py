
#print("simple_test")

# Lancer Sofa 3 fois avec une scène vide ?

import Sofa
import time
import SimuVal as Sim
import viewer_Sofa as viewer

from ucl_collaboration import *
from ucl_collaboration.stiff_module import *

# from Scene_exemple import *

### PARAM7TRES ####

# nb_scene = 3

###################

global step 
global iter_sim

step = 0
iter_sim = 0

for i in range(Sim.nb_scene):
    print("\n Scene SOFA  n :  " + str(i))
            
    # SOFA simulation
    root = Sofa.Core.Node("root") # Generate the root node 
    # MyScene(root,step) # for Scene_exemple 
    MyScene(root,out_flag = 1,step =step) # Create the scene graph
    Sofa.Simulation.init(root) # Initialization of the scene graph
    viewer.init_display(root)

    # print(dir(root))
    
    # Don't do anything until end of simulation
    while root.simu_controller.FinSimu != True: #SImuDOne is a variable udpated with the controller when the simulation is finished
         Sofa.Simulation.animate(root, root.dt.value)
         Sofa.Simulation.updateVisual(root)
         viewer.simple_render(root)
         time.sleep(root.dt.value)
         iter_sim += 1   
    
    # Reset simulation
    Sofa.Simulation.reset(root)
    
    # Load next PCL
    step += 1  