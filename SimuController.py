import Sofa
import SimuVal as Sim
from spicy import *
from os import getcwd, chdir, mkdir
from datetime import datetime
import csv
import time



""" Controller for simulation """
class simu_controller(Sofa.Core.Controller):

    def __init__(self,step,*args, **kwargs):
        Sofa.Core.Controller.__init__(self, *args, **kwargs)
        
        self.RootNode = kwargs["RootNode"]
        
        self.IterSimu = 0 # Counter for dt steps before stopping simulation
        self.MaxIter = Sim.nb_iter[step] # Number of dt steps before stopping simulation
        self.FinSimu = False # Variable to store if Simulation is done
        self.pression_d = Sim.pression_d
        # self.pas_pres = Sim.pas_pres
        self.pas_pres = self.pression_d / self.MaxIter
        # self.pression = 0#relier à la vraie pression dans les chambres
    
        
    def onAnimateBeginEvent(self, dt): 

        # Compute error when simu is done            
        if (self.IterSimu >= self.MaxIter-1) :
            # self.compute_error()
            # with open(constants.OptimPath + "logs.txt","a") as logs:
            #     logs.write("\n")
            self.FinSimu = True 
        self.IterSimu += 1

class position_controller(Sofa.Core.Controller):

    def __init__(self, nb_module,step,nb_cavity,*args, **kwargs):
        Sofa.Core.Controller.__init__(self, *args, **kwargs)
        
        self.RootNode = kwargs["RootNode"]
        
        self.MaxIter = Sim.nb_iter[step] # Number of dt steps before stopping simulation
        self.pression_d = Sim.pression_d
        self.pas_pres = self.pression_d / self.MaxIter

        self.stiffNode=self.RootNode.getChild('RigidFrames')

        ind = -1
        self.pressure = []
        print('-------')
        print('SIMU : Noeuds connectés au Controller : ')
        for i in range(nb_module):
            for j in range(nb_cavity):
                ind = ind + 1
                node_name = "Bellow" + str(j+1) + str(i+1)            
                self.noeud = self.stiffNode.getChild(node_name)
                # self.noeud.append(self.stiffNode.getChild(node_name)) # finalement pas de liste de noeuds
                print(node_name)
                self.pressure.append(self.noeud.getObject('SPC'))

        print('-------')
        print(' ')

        self.flag = 0
        self.nb_cavity = nb_cavity
        self.nb_module = nb_module
    
        
    def onAnimateBeginEvent(self, dt): 

        pressureValue = zeros(self.nb_cavity)

        # print("aouai ??????")
        # print(self.flag)
        # print("peut etre apres tt !!!!!!!")
        # print('flag : ' + str(self.flag) + 'nb_cavity : ' + str(self.nb_cavity))
        # print("anon !!!!!!!")

        index = self.flag*self.nb_cavity
        for i in range(self.nb_cavity):
            pressureValue[i] = self.pressure[index+i].value.value[0]

        # print("iciossi?")

        pressureValue[0] += self.pas_pres
        # print("SIMU : Pression chambre", str(pressureValue[0]))

        print('         ****       ')
        print('SIMU : Control du mondule n° : ',self.flag+1)
        for i in range(self.nb_cavity): # remise valeurs au bon endroit
            self.pressure[index+i].value = [pressureValue[i]]
            print('SIMU : Pression chambre ',i,' : ',pressureValue[i])
        print('         ****       ')


class CsvPositionController(Sofa.Core.Controller):

    def __init__(self, nb_module,nb_cavity,step,*args, **kwargs):
        Sofa.Core.Controller.__init__(self, *args, **kwargs)
        
        self.RootNode = kwargs["RootNode"]
        self.step = step
        
        self.MaxIter = Sim.nb_iter[step] # Number of dt steps before stopping simulation
        self.IterSimu = 0 # Counter for dt steps before stopping simulation

        self.stiffNode=self.RootNode.getChild('RigidFrames')

        ind = -1
        self.pressure = []
        print('-------')
        print('SIMU : Noeuds connectés au Controller : ')
        for i in range(nb_module):
            for j in range(nb_cavity):
                ind = ind + 1
                node_name = "Bellow" + str(j+1) + str(i+1)            
                self.noeud = self.stiffNode.getChild(node_name)
                # self.noeud.append(self.stiffNode.getChild(node_name)) # finalement pas de liste de noeuds
                print(node_name)
                self.pressure.append(self.noeud.getObject('SPC'))
                # self.pressure.append(self.noeud[ind].getObject('SPC'))

        print('-------')
        print(' ')

        self.flag = 0
        self.nb_cavity = nb_cavity
        self.nb_module = nb_module
    
        
    def onAnimateBeginEvent(self, dt): 

        # global step
        # global iter_sim

        pressureValue = zeros(self.nb_cavity)

        index = self.flag*self.nb_cavity
        for i in range(self.nb_cavity):
            pressureValue[i] = self.pressure[index+i].value.value[0]

        # print(iter_sim)

        # print("iciossi?")

        pressureValue[0] = Sim.tab[self.step][self.IterSimu][1] # pression a droite dans le
        # print("SIMU : Pression chambre", str(pressureValue[0]))
        # print("mais_pasicijepresume?")

        # print('         ****       ')
        # print('SIMU : Control du mondule n° : ',self.flag+1)
        for i in range(self.nb_cavity): # remise valeurs au bon endroit
            self.pressure[index+i].value = [pressureValue[i]]
        #     print('SIMU : Pression chambre ',i,' : ',pressureValue[i])
        # print('         ****       ')
        self.IterSimu += 1

class PositionComparator(Sofa.Core.Controller):
    def __init__(self,step,module,*args, **kwargs):
        Sofa.Core.Controller.__init__(self,*args,**kwargs)
        self.RootNode = kwargs["RootNode"]
        self.stiffNode = self.RootNode.getChild('RigidFrames')
        self.position = self.stiffNode.getObject('DOFs')
        self.nb_poutre = module.nb_poutre
        self.nb_module = module.nb_module
        self.nb_cavity = module.nb_cavity
        self.step = step
        self.IterSimu = 0 # Counter for dt steps before stopping simulation
        self.ecart = 0 # ecart entre la simulation et la réalité, en mm


    def onAnimateBeginEvent(self, dt): 
        pos_raw = self.position.position.value[self.nb_poutre-1][0:3]
        pos = pos_raw # on ne remet pass dans le repère de l'effecteur
        # pos = pos_raw - Sim.pos_i
        print(pos)
        distance = sqrt(pos[0]*pos[0]+pos[2]*pos[2])
        # print('Distance simu : '+ str(distance))
        distance_d = Sim.tab[self.step][self.IterSimu][0] # distance désiré entre la position de repos et la position actuelle du robot, enregistrés dans les csv
        print('Distance expérience :'+str(distance_d))
        # print('testetsttst2')
        # print(type(distance))
        # print(type(distance_d))
        self.ecart = abs(float(distance_d)-distance)
        self.IterSimu += 1
        # print('Ecart entre les 2 : ' + str(self.ecart))



# ### - CSV PRINTER - ###
class SimuPrinterCsv(Sofa.Core.Controller):
    def __init__(self,module,*args, **kwargs):
        Sofa.Core.Controller.__init__(self,args,kwargs)
        self.node = args[0]
        self.stiffNode = self.node.getChild('RigidFrames')
        self.position = self.stiffNode.getObject('DOFs')
        self.nb_poutre = module.nb_poutre
        self.nb_module = module.nb_module
        self.nb_cavity = module.nb_cavity

        txt_chmbre = ""

        ind = -1
        self.pressure = []
        print('-------')
        print('SIMU : Noeuds connectés au Printer Csv : ')
        for i in range(self.nb_module):
            txt_chmbre = txt_chmbre + ' , [Pression module n' + str(i+1) + '] , [Volume module n' + str(i+1) + ']'
            for j in range(self.nb_cavity):
                ind = ind + 1
                node_name = "Bellow" + str(j+1) + str(i+1)            
                self.noeud = self.stiffNode.getChild(node_name)
                # self.noeud.append(self.stiffNode.getChild(node_name)) # finalement pas de liste de noeuds
                print(node_name)
                self.pressure.append(self.noeud.getObject('SPC'))
                # self.pressure.append(self.noeud[ind].getObject('SPC'))
        print('-------')
        print(' ')

        self.path = getcwd()
        d_et_h = str(datetime.now())
        self.nf = self.path+'/record/pos_stiff_record_' + d_et_h[0:19] + '.csv'
        self.fichier_csv = open(self.nf,'a')

        self.fichier_csv.write('Caracteristiques des modules de la simulation : ')
        self.fichier_csv.write('\n Hauteur du module (en mm) : ,' + str(module.h_module))
        self.fichier_csv.write('\n Pression initiale : ,' + str(module.init_pressure_value))
        # self.fichier.write('Hauteur du module, en mm :', str(module.value_type))
        self.fichier_csv.write('\n Module de Young des parties souples : ,' + str(module.YM_soft_part))
        self.fichier_csv.write('\n Module de Young des parties rigides : ,' + str(module.YM_stiff_part))
        self.fichier_csv.write('\n Coefficient de Poisson : ,' + str(module.coef_poi))
        self.fichier_csv.write('\n Nombre de modules robotiques : ,'+ str(module.nb_module))
        self.fichier_csv.write('\n Nombre de paires de cavites par module : ,'+ str(module.nb_cavity))
        self.fichier_csv.write('\n Nombre de poutres le long de l ensemble des modules : ,'+ str(module.nb_poutre))
        self.fichier_csv.write('\n Pression maximale : ,'+ str(module.max_pression))
        self.fichier_csv.write('\n Masse d un module (en kg) : ,'+ str(module.masse_module))
        if module.rigid_bool == 1 :
            self.fichier_csv.write('\n Application des parties rigides : ,'+ str(module.rigid_bool))
        self.fichier_csv.write('\n Modele 3D des modules : ,'+ str(module.module_model))
        self.fichier_csv.write('\n Modele 3D des chambres : ,'+ str(module.chamber_model))
        # self.fichier_csv.write('\n')
        self.fichier_csv.write('\n [Positions effecteur] , [Temps relatif] ' + txt_chmbre + '\n')

        self.start = time.time()

    def onAnimateBeginEvent(self, dt): 
        # print(self.nf)
        pos = self.position.position.value[self.nb_poutre-1][0:3]
        ind = 0
        # pres = []
        # print(str(time.time() - self.start)) 
        time_txt = ", [" + str(time.time() - self.start) + "]"
        pres_txt = ""
        for i in range(self.nb_module):
            pres_txt = pres_txt + ",["
            i0 = ind
            for j in range(self.nb_cavity):
                pres_txt = pres_txt + ' ' + str(self.pressure[ind].value.value[0])
                ind = ind + 1
            pres_txt = pres_txt + "]"
            ind = i0
            pres_txt = pres_txt + ",["
            for j in range(self.nb_cavity):
                pres_txt = pres_txt + ' ' + str(self.pressure[ind].cavityVolume.value)
                ind = ind + 1
            pres_txt = pres_txt + "]"

        self.fichier_csv.write(str(pos) + time_txt + pres_txt +'\n')

        # print('testpass')

        # if e["key"] == Key.Q :
        # if self.end == True:
        self.fichier_csv.close()
        print("%%%% Positions Enregistrées en Csv %%%%")

        self.fichier_csv = open(self.nf,'a')

        # print("testagain")



