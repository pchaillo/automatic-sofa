
#print("simple_test")

# Lancer Sofa 3 fois avec une scène vide ?

import Sofa
import time
import SimuVal as Sim
import viewer_Sofa as viewer
import numpy as np
from scipy.spatial import distance
from os import getcwd, chdir, mkdir
from datetime import datetime
import time

from ucl_collaboration import *
from ucl_collaboration.stiff_module import *

# from Scene_exemple import *

### PARAM7TRES ####

# nb_scene = 3

###################

def scene_autosofa(param_YM,param_CP):
    step = 0
    iter_sim = 0
    ecart_tab = []

    for i in range(Sim.nb_scene):
        print("\n Scene SOFA  n :  " + str(i))
                
        # SOFA simulation
        root = Sofa.Core.Node("root") # Generate the root node 
        # MyScene(root,step) # for Scene_exemple 
        MyScene(root,out_flag = 1,step =step,YM_soft_part = param_YM,coef_poi = param_CP) # Create the scene graph
        Sofa.Simulation.init(root) # Initialization of the scene graph
        viewer.init_display(root)

        # print(dir(root))
        
        # Don't do anything until end of simulation
        while root.simu_controller.FinSimu != True: #SImuDOne is a variable udpated with the controller when the simulation is finished
             Sofa.Simulation.animate(root, root.dt.value)
             Sofa.Simulation.updateVisual(root)
             viewer.simple_render(root)
             time.sleep(root.dt.value)
             iter_sim += 1  

             # print('~~~~~~~~~~~~' + str(root.simu_pos_compar.ecart)) 
             ecart_tab.append(root.simu_pos_compar.ecart)
                
        # Reset simulation
        Sofa.Simulation.reset(root)
        step += 1  
        # print('Tableau de tous les écart enregistrés : ' + str(ecart_tab))
    return ecart_tab


class best_param_recorder():
    def __init__(self):
        self.path = getcwd()
        d_et_h = str(datetime.now())
        self.nf = self.path+'/record/best_param_record_' + d_et_h[0:19] + '.txt'
        self.fichier_txt = open(self.nf,'a')

    def record_best(self,best_param_a,delta_a,best_param_b,delta_b,i_boucle,pos):
        self.fichier_txt.write('\n ----------- Boucle numéro : ' + str(i_boucle) + ' ---------')
        self.fichier_txt.write('\n Nouveau paramètre a : ' + str(best_param_a))
        self.fichier_txt.write('\n delta a : ' + str(delta_a))
        self.fichier_txt.write('\n Nouveau paramètre b : ' + str(best_param_b))
        self.fichier_txt.write('\n delta b : ' + str(delta_b))
        self.fichier_txt.write('\n Erreur mesurée: ' + str(pos))
        self.fichier_txt.write('\n -----------  ---------')
        self.fichier_txt.close()
        self.fichier_txt = open(self.nf,'a')


def best_param_printer(best_param_a,delta_a,best_param_b,delta_b,pos,i):
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("~~~" + str(i))
    print("~~~ Nouveau paramètre a : " + str(best_param_a)+ "~~~")
    print("~~~ delta a : " + str(delta_a)+ "~~~")
    print("~~~ Nouveau paramètre b : " + str(best_param_b)+ "~~~")
    print("~~~ delta b : " + str(delta_b)+ "~~~")
    print("~~~ Erreur mesurée :" + str(pos))
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

def erreur_recup(ecart_tab): # détermine l'erreur que je vais prendre en compte par la suite, dans une fonction à part pour être sûr que ce soit la même méthode utilisée partout
    pos_new = np.mean(ecart_tab)
    # pos_new = np.median(ecart_tab)
    # pos_new = max(ecart_tab)

    return pos_new

# def derive(pos_i,pos_new,delta):
    # deriv = (pos_i - pos_new)/delta
    # return deriv

def gradient_param(pos_i,delta,param_YM,param_CP):
    # descente de gradient selon un paramètre
    ecart_tab = scene_autosofa(param_YM,param_CP)
    pos_new = erreur_recup(ecart_tab)
    deriv = (pos_i - pos_new)/delta
    return deriv

def descente_gradient(pos,erreur_d,param_a,param_b, delta_a,delta_b,record,alpha_a,alpha_b):
    i = 0
    while pos > erreur_d :
        i = i + 1
        deriv_a = gradient_param(pos_i = pos,delta = delta_a, param_YM = param_a + delta_a,param_CP = param_b)
        deriv_b = gradient_param(pos_i = pos,delta = delta_b, param_YM = param_a ,param_CP = param_b + delta_b )
        # deriv_b = 0 # pour faire varier seulement a, decommenter cette ligne et commenter la précedente
        delta_a = deriv_a*alpha_a
        delta_b = deriv_b*alpha_b
        param_a = param_a + delta_a
        param_b = param_b + delta_b
        ecart_tab = scene_autosofa(param_YM = param_a,param_CP = param_b)
        pos = erreur_recup(ecart_tab)
        best_param_printer(param_a,delta_a,param_b,delta_b,pos,i)
        record.record_best(param_a,delta_a,param_b,delta_b,i,pos)
    return [param_a, param_b, pos]


####################################################################################################
# start_time = time.time()

param_flag = 0 # 0 => YM // 1 => coef_poi

param_a = 41.11 # paramètre initial
param_b = 0.3870
ecart_tab = scene_autosofa(param_YM = param_a,param_CP=param_b)
pos = erreur_recup(ecart_tab) # valeur/position de la fonction initiale
# pos_d = 0 # position désirée
delta_a = - 10 # pas initial
delta_b = 0.1
erreur_d = 1 # l'algo s'arrête lorsqu'on atteint une valeur sous ce seuil => erreur désirée
# dist_best = pos_i
# nb_iter = 0
# nb_iter_max = 10
# nb_boucle = 2 # nb d boucle Ym puis Cp à effectuer (2=1ym+1cp)
alpha_a = 10 # taux d'apprentissage
alpha_b = 0.0005

record = best_param_recorder()

print("Ecart initial : " + str(pos))
# parametre_estime = reccur_parametre(param_i,pos_i,pos_d, delta_i,erreur,nb_iter,nb_iter_max)#,dist_best)
[param_a,param_b,erreur_f] = descente_gradient(pos,erreur_d,param_a,param_b, delta_a,delta_b,record,alpha_a,alpha_b)
# print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
# print("~~~ Nouveau paramètre : " + str(parametre_estime)+ "~~~")
# print("~~~ Erreur acceptée : " + str(erreur)+ "~~~")
# print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

# best_param_printer(param_a,best_erreur_a,param_b,best_erreur_b)

# interval = time.time() - start_time
# print('Total time in seconds:'+ str(interval) )