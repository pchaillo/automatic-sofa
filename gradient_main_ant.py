
import Sofa
import SofaRuntime
import numpy as np
import time

from parameters import *
from echelon3 import *


###################
# Sofa Scene
###################
SofaRuntime.PluginRepository.addFirstPath('/home/alessandrini/Repos/Code/sofa/build/external_directories')

def scene_autosofa(param_YR,param_YT):
    step = 0
    iter_sim = 0
    poseSim = []

    for i in range(nb_scene):
        print("\n Scene SOFA  n :  " + str(i))
                
        # SOFA simulation
        rootNode = Sofa.Core.Node("root") # Generate the root node 
        createScene(rootNode,YoungModulusRibs = param_YR,YoungModulusTube = param_YT) # Create the scene graph
        Sofa.Simulation.init(rootNode) # Initialization of the scene graph


        # Don't do anything until end of simulation
        for iteration in range(100):
            Sofa.Simulation.animate(rootNode, rootNode.dt.value)
            Sofa.Simulation.updateVisual(rootNode)
            iter_sim += 1  
        
        poseSim = np.array(rootNode.Echelon.framesNode.frames.position.value[-1])
        # Reset simulation
        Sofa.Simulation.reset(rootNode)
        step += 1  

    return poseSim

###################
# Gradiant function
###################

def gradient_param(error,delta,param_YM,param_CP,poseReal):
    # descente de gradient selon un paramètre
    poseSim = scene_autosofa(param_YM,param_CP)
    error_new =  poseSim[2]-poseReal[2]
    deriv = (error-error_new)/delta
    return deriv

def descente_gradient(error,erreur_d,param_a,param_b, delta_a,delta_b,alpha_a,alpha_b,poseReal):
    i = 0
    while error > erreur_d :
        i = i + 1
        deriv_a = gradient_param(error = error,delta = delta_a, param_YM = param_a + delta_a,param_CP = param_b, poseReal = poseReal)
        deriv_b = gradient_param(error = error,delta = delta_b, param_YM = param_a ,param_CP = param_b + delta_b ,poseReal = poseReal)
        # deriv_b = 0 # pour faire varier seulement a, decommenter cette ligne et commenter la précedente
        delta_a = deriv_a*alpha_a
        delta_b = deriv_b*alpha_b
        param_a = param_a + delta_a
        param_b = param_b + delta_b
        print(param_a,param_b)
        poseSim =  scene_autosofa(param_YR = param_a,param_YT=param_b)
        print(poseSim)
        error = poseSim[2]-poseReal[2]
        print(error)
    return [param_a, param_b, error]


####################################################################################################
# # start_time = time.time()

param_a = 3.5e8 # paramètre initial
param_b = 1e6
poseSim = scene_autosofa(param_YR = param_a,param_YT=param_b)
poseReal = [0,0,-714]
error = poseSim[2]-poseReal[2]

delta_a = - 0.001 # pas initial
delta_b = 0.001
error_min = 1 # l'algo s'arrête lorsqu'on atteint une valeur sous ce seuil => erreur désirée

alpha_a = 1e9 # taux d'apprentissage
alpha_b = 1e9


[param_a,param_b,erreur_f] = descente_gradient(error,error_min,param_a,param_b, delta_a,delta_b,alpha_a,alpha_b,poseReal)
